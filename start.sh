docker network create lemp

docker run -d --name db -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress -e MYSQL_DATABASE=wordpress -v "$(pwd)/db:/var/lib/mysql" --net lemp mysql:5.6

docker run -d --name web_fpm -v "$(pwd)/wordpress:/var/www/html" --net lemp mishraamrish/lemp-php:1.0

docker run -d --name web -p 80:80 -v "$(pwd)/wordpress:/var/www/html:rw" -v "$(pwd)/nginx/default:/etc/nginx/sites-available/default:ro" -v "$(pwd)/nginx/logs:/var/log/nginx:rw" --net lemp mishraamrish/lemp-nginx:1.0

