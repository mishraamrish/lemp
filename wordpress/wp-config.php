<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'db' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm(P;qgR#gzs[[X]lW+IV-n7|[P}A!aQG%_WI+dpM-LBt|+*MU[u3E/F|%mc`)ZlV');
define('SECURE_AUTH_KEY',  '){<;vW:pexB+W:<-m~@51uq^>GFR+3`m|)$cZH;+g;_jl(-dEtKkxxB.}Zu&HwtT');
define('LOGGED_IN_KEY',    '>F9=51JC&$t-zz|8<1fp^kiUjP5doYBe5eKB2Nlj5$X]Z8FjV@O8kr[W{Xe{K!,g');
define('NONCE_KEY',        'i}!Mf?::pJrR*C:4-p(o{p>(+XRo15u:=L)|XQp,EFWA6|3|5Y~;=9,O-pd;`H`+');
define('AUTH_SALT',        '_ZW_ik L17*<l:3bYapy-G10A336Tol}-I(b0 XS)E]hn!Re7mGw$*i2b.fAaJGD');
define('SECURE_AUTH_SALT', 'c9iMB@($b nB5U `w%W|UVrR*m1M;BW!7tQ3Vz-DAw/sk%Fud-K-x|RaR{Q_#t_j');
define('LOGGED_IN_SALT',   'ii<gu}tzQVt#%]#A<;G]PqE4- B?{Js/Z(N0sB*b4;5]t>`Jn3{=@keQ4E$|JNIm');
define('NONCE_SALT',       '[#UZtiSRuZx0{3=g*Ew%C&QJX#{5[@}lu TkPE9^u=g<_$#?l:n)bCgpI$R&X0l7');
define('FS_METHOD', 'direct');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
